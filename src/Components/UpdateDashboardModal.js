import { useState } from "react";
const UpdateDashboardModal = ({
  handleCloseUpdateDashboardModal,
  projectPayLoad,
  goToDashboardSubmit,
}) => {
  const [startdate, setStartdate] = useState(projectPayLoad.startDate);
  const [enddate, setEnddate] = useState(projectPayLoad.endDate);
  //   console.log("----->", startdate, enddate);

  const handleRequest = () => {
    const load = {
      projectName: projectPayLoad.projectName,
      startDate: startdate,
      endDate: enddate,
    };
    goToDashboardSubmit(load);
    handleCloseUpdateDashboardModal();
  };
  return (
    <>
      <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
        <div className=" h-[345px] w-[645px] text-black p-10 bg-white  rounded-[20px] shadow flex flex-col gap-4">
          <div
            style={{
              fontSize: "18px",
              fontWeight: "600",
              lineHeight: "16px",
            }}
          >
            {projectPayLoad.projectName}
          </div>
          <div className="flex flex-row w-full h-full gap-4">
            <div className="w-[50%]">
              <div className="w-full ">
                <label
                  for="projectinitiationdate"
                  class="flex-1 block text-black p-1"
                  style={{
                    fontSize: "14px",
                    fontWeight: "600",
                    lineHeight: "18px",
                  }}
                >
                  Project Initiation Date
                </label>
                <div className="flex flex-row items-center w-full p-1 bg-white border-[1px] border-[#CCCCCC] rounded-xl justify-normal">
                  <input
                    type="date"
                    id="projectinitiationdate"
                    class="bg-white h-10  text-black font-bold text-sm block  p-2 w-full placeholder:text-[#00000066] placeholder:font-medium "
                    placeholder=""
                    value={startdate}
                    onChange={(e) => setStartdate(e.target.value)}
                    required
                  />
                </div>
              </div>
            </div>
            <div className="w-[50%]">
              <div className="w-full ">
                <label
                  for="projectterminationdate"
                  class="flex-1 block text-black p-1"
                  style={{
                    fontSize: "14px",
                    fontWeight: "600",
                    lineHeight: "18px",
                  }}
                >
                  Project Termination Date
                </label>
                <div className="flex flex-row items-center w-full p-1 bg-white border-[1px] border-[#CCCCCC] rounded-xl justify-normal">
                  <input
                    type="date"
                    id="projectterminationdate"
                    class="bg-white h-10  text-black font-bold text-sm block  p-2 w-full placeholder:text-[#00000066] placeholder:font-medium "
                    placeholder=""
                    value={enddate}
                    onChange={(e) => setEnddate(e.target.value)}
                    required
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="flex justify-end ">
            <button
              onClick={handleRequest}
              className="p-4 bg-[#5F085A] text-white rounded-xl"
            >
              Submit Details
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default UpdateDashboardModal;
