import DashboardCost from "./DashboardCost";
import DashboardActivity from "./DashboardActivity";
import DashboardHeader from "./DashboardHeader";
const DashboardView = ({
  goToCost,
  cost,
  data,
  goToActivity,
  activity,
  projectPayLoad,
  sliderData
}) => {
  return (
    <>
      <div className="flex flex-col w-full">
        <div className="flex flex-row justify-between">
          <DashboardHeader
            activity={activity}
            goToCost={goToCost}
            cost={cost}
            goToActivity={goToActivity}
            
            projectPayLoad={projectPayLoad}
          ></DashboardHeader>
        </div>
        <div className="w-full pt-5">
          {cost ? (
            <>
              <DashboardCost data={data} sliderData={sliderData}></DashboardCost>
            </>
          ) : (
            <></>
          )}
          {activity ? (
            <>
              <DashboardActivity data={data}></DashboardActivity>
            </>
          ) : (
            <></>
          )}
        </div>
      </div>
    </>
  );
};

export default DashboardView;
