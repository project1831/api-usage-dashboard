const DashboardHeader = ({
  projectPayLoad,
  activity,
  goToCost,
  cost,
  goToActivity,

}) => {
  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0"); // Months are 0-indexed
    const year = date.getFullYear().toString().slice(-2); // Get last two digits of year

    return `${day}.${month}.${year}`;
  };
  return (
    <>
      <div className="flex flex-row w-[40%] gap-2 ">
        <button
          onClick={goToCost}
          style={{
            fontSize: "16px",
            fontWeight: "600",
            lineHeight: "24px",
          }}
          class={`relative inline-block p-4 w-[40%] rounded-[32px] ${
            cost
              ? "bg-[#5F085A] text-white"
              : "bg-[#FAFAFA] text-[#5F085A] border-[1px] border-[#5F085A]"
          }  font-semibold rounded`}
        >
          Cost
          {cost ? (
            <>
              <span class="absolute bottom-0 left-1/2 transform -translate-x-1/2">
                <span class="absolute h-2 w-2 bg-[#5F085A] bottom-0 -mb-1 rotate-45"></span>
              </span>
            </>
          ) : (
            <></>
          )}
        </button>
        <button
          onClick={goToActivity}
          class={`relative inline-block p-4 w-[40%] rounded-[32px] ${
            activity
              ? "bg-[#5F085A] text-white"
              : "bg-[#FAFAFA] text-[#5F085A] border-[1px] border-[#5F085A]"
          }  font-semibold rounded`}
        >
          Activity
          {activity ? (
            <>
              <span class="absolute bottom-0 left-1/2 transform -translate-x-1/2">
                <span class="absolute h-2 w-2 bg-[#5F085A] bottom-0 -mb-1 rotate-45"></span>
              </span>
            </>
          ) : (
            <></>
          )}
        </button>
      </div>
      <div className="flex flex-row gap-4 justify-evenly">
        {activity ? (
          <>
            <div className="flex items-center justify-center">
              <select
                id="countries"
                placeholder="Model"
                class="bg-[#FAFAFA] border border-[#5F085A] text-[#5F085A] font-semibold rounded-lg   w-full p-2.5  "
              >
                <option selected>Models</option>
                <option value="US">United States</option>
                <option value="CA">Canada</option>
                <option value="FR">France</option>
                <option value="DE">Germany</option>
              </select>
            </div>
          </>
        ) : (
          <></>
        )}
        <div className="flex flex-row items-center justify-center ">
          <div
            className="px-5 py-3 text-white bg-black rounded-[32px]"
            style={{
              fontSize: "14px",
              fontWeight: "500",
              lineHeight: "16px",
            }}
          >
            {formatDate(projectPayLoad.startDate)} -{" "}
            {formatDate(projectPayLoad.endDate)}
          </div>
        </div>
        <div className="flex items-center justify-center">
          <button
            className="px-5 py-3 text-white bg-[#5F085A] rounded-[32px]"
            style={{
              fontSize: "14px",
              fontWeight: "500",
              lineHeight: "16px",
            }}
          >
            Export
          </button>
        </div>
      </div>
    </>
  );
};
export default DashboardHeader;
