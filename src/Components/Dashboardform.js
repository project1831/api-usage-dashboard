import { useState, useEffect } from "react";
import axios from "axios";

const Dashboardform = ({ goToDashboardSubmit }) => {
  const [name, setName] = useState("");
  const [startdate, setStartdate] = useState("");
  const [enddate, setEnddate] = useState("");

  const [isOpen, setIsOpen] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");

  const [items, setItems] = useState([]);
  const [filteredItems, setFilteredItems] = useState([]);
  // console.log(filteredItems)
  const token = localStorage.getItem("token")

  const fetchData = async () => {
    try {
      const response = await axios.post(
        "https://api-usage-dashboard-backend-python.azurewebsites.net/get-projects-names",
        { adminEmailId: "bishwayan.saha@pwc.com" },
        {
          headers: {
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`,
          },
        }
      );
      // console.log(response.data.projects);
      setItems(response.data.projects);
      setFilteredItems(response.data.projects);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  // Call fetchData on component mount
  useEffect(() => {
    fetchData();
  });

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleInputChange = (e) => {
    const term = e.target.value.toLowerCase();
    setSearchTerm(term);

    // Filter items based on the search term
    const filtered = term
      ? items.filter((item) => item.toLowerCase().includes(term))
      : items;

    setFilteredItems(filtered);
  };

  const nameChange = (data) => {
    setName(data);
    // console.log(name);
    setIsOpen(false);
  };

  const startChange = (e) => {
    setStartdate(e.target.value);
  };

  const endChange = (e) => {
    setEnddate(e.target.value);
  };

  const payLoad = {
    startDate: startdate,
    endDate: enddate,
    projectName: name,
  };

  // useEffect(()=>{
  //     const fetchData = async () =>{
  //     var formData = {
  //       "projectName":name,
  //       "startDate":startdate,
  //       "endDate":enddate
  //     }
  //     setFormDatastate(formData);

  //     }
  //     fetchData();
  //   },[name, startdate, enddate])

  const handleDashboardSubmit = (e) => {
    goToDashboardSubmit(payLoad);
  };

  return (
    <>
      <div className="flex flex-col justify-between gap-6">
        <div
          style={{
            fontSize: "19px",
            fontWeight: "600",
            lineHeight: "23.16px",
          }}
        >
          Enter Your Project Details To Continue
        </div>
        <div>
          <div className="w-full ">
            <div className="flex ">
              <div className="relative w-full group">
                <button
                  id="dropdown-button"
                  onClick={toggleDropdown}
                  className="inline-flex justify-between w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-blue-500"
                >
                  <span
                    className="mr-2"
                    style={{
                      fontSize: "16px",
                      fontWeight: "600",
                      lineHeight: "23.16px",
                    }}
                  >
                    {name ? <>{name}</> : <>Enter Project Name</>}
                  </span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M3.75 3v11.25A2.25 2.25 0 0 0 6 16.5h2.25M3.75 3h-1.5m1.5 0h16.5m0 0h1.5m-1.5 0v11.25A2.25 2.25 0 0 1 18 16.5h-2.25m-7.5 0h7.5m-7.5 0-1 3m8.5-3 1 3m0 0 .5 1.5m-.5-1.5h-9.5m0 0-.5 1.5M9 11.25v1.5M12 9v3.75m3-6v6"
                    />
                  </svg>
                </button>
                <div
                  id="dropdown-menu"
                  className={`${
                    isOpen ? "" : "hidden"
                  } w-full absolute right-0 mt-2 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 p-1 space-y-1`}
                >
                  {/* Search input */}
                  <div className="flex flex-row px-4 py-2 text-gray-800 border border-gray-300 rounded-md focus:outline-none">
                    <input
                      id="search-input"
                      className="w-full "
                      type="text"
                      placeholder="Search items"
                      autoComplete="off"
                      value={searchTerm}
                      onChange={handleInputChange}
                    />
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-8 h-8"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="m21 21-5.197-5.197m0 0A7.5 7.5 0 1 0 5.196 5.196a7.5 7.5 0 0 0 10.607 10.607Z"
                      />
                    </svg>
                  </div>
                  {/* Display filtered items */}
                  {filteredItems.map((item, index) => (
                    <div
                      key={index}
                      onClick={() => nameChange(item)}
                      className="block px-4 py-2 text-gray-700 rounded-md cursor-pointer hover:bg-gray-100 active:bg-blue-100"
                    >
                      {item}
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-row gap-4">
          <div className="w-[50%]">
            <div className="w-full ">
              <label
                for="projectinitiationdate"
                class="flex-1 block text-[#666666] p-1"
                style={{
                  fontSize: "14px",
                  fontWeight: "600",
                  lineHeight: "18px",
                }}
              >
                Project Initiation Date
              </label>
              <div className="flex flex-row items-center w-full p-1 bg-white border-[1px] border-[#CCCCCC] rounded-xl justify-normal">
                <input
                  type="date"
                  id="projectinitiationdate"
                  class="bg-white h-10  text-[#666666] font-bold text-sm block  p-2 w-full placeholder:text-[#00000066] placeholder:font-medium "
                  placeholder=""
                  required
                  onChange={startChange}
                />
              </div>
            </div>
          </div>
          <div className="w-[50%]">
            <div className="w-full ">
              <label
                for="projectterminationdate"
                class="flex-1 block text-[#666666] p-1"
                style={{
                  fontSize: "14px",
                  fontWeight: "600",
                  lineHeight: "18px",
                }}
              >
                Project Termination Date
              </label>
              <div className="flex flex-row items-center w-full p-1 bg-white border-[1px] border-[#CCCCCC] rounded-xl justify-normal">
                <input
                  type="date"
                  id="projectterminationdate"
                  class="bg-white h-10  text-[#666666] font-bold text-sm block  p-2 w-full placeholder:text-[#00000066] placeholder:font-medium "
                  placeholder=""
                  required
                  onChange={endChange}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="flex justify-end">
          <button
            onClick={handleDashboardSubmit}
            className="w-[23%] py-[1.5%] bg-[#5F085A] text-white rounded-3xl"
          >
            Submit Details
          </button>
        </div>
      </div>
    </>
  );
};
export default Dashboardform;
