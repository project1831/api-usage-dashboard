import MixBarchart from "./MixBarChart";
import Areachart from "./AreaChart";

const DashboardActivity = ({ data }) => {
  //for gpt data
  // const gptData = data["gpt-3.5-turbo-0125"];
  // const gptTokens = gptData.reduce((total, item) => total + item.tokens, 0);
  // const categories = [
  //   {
  //     category: gptData.map((item) => ({ label: item.date })),
  //   },
  // ];
  // // Function to convert date format
  function convertDateFormat(dateString) {
    var dateParts = dateString.split("-");
    var year = dateParts[0];
    var month = dateParts[1];
    var day = dateParts[2];

    var monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];

    var monthName = monthNames[parseInt(month) - 1];

    return day + monthName + "," + year;
  }
  // categories.forEach(function (item) {
  //   item.category.forEach(function (dateItem) {
  //     dateItem.label = convertDateFormat(dateItem.label);
  //   });
  // });
  // const datas = [
  //   {
  //     data: gptData.map((item) => ({ value: item.requests })),
  //   },
  // ];

  // console.log(datas, "dtscheck");


  return (
    <>
      <div className="grid h-full grid-cols-2 gap-6 ">
        {console.log(data)}
        {
          
          Object.entries(data).map(([key, arrayData], index) => (
            <>
            <div className=" w-fit bg-white rounded-[20px] p-6  flex flex-col ">
          <span
            style={{
              fontSize: "20px",
              fontWeight: "600",
              lineHeight: "32px",
            }}
            className="text-[#05004E]"
          >
            {key.split(":")[0].toUpperCase()}
          </span>
          <Areachart category={[
                        {
                          category: arrayData.map((item) => ({
                            label: convertDateFormat(item.date),
                          })),
                        },
                      ]} data={[
                        { data: arrayData.map((item) => ({ value: item.cost })) },
                      ]}></Areachart>
        </div>
        <div className="w-fit  bg-white rounded-[20px] p-6 flex items-end flex-col">
          <div
            className="px-5 py-2 mr-3 w-[35%]  text-white bg-[#AA0DA1] rounded-[32px]"
            style={{
              fontSize: "14px",
              fontWeight: "500",
              lineHeight: "16px",
            }}
          >
            Tokens : {arrayData.reduce((total, item) => total + item.tokens, 0)}
          </div>
          <MixBarchart
            category={[
              {
                category: arrayData.map((item) => ({
                  label: convertDateFormat(item.date),
                })),
              },
            ]}
            data={[
              { data: arrayData.map((item) => ({ value: item.cost })) },
            ]}
            wid={false}
          ></MixBarchart>
        </div></>
          ))
        }
        
        
      </div>
    </>
  );
};
export default DashboardActivity;
