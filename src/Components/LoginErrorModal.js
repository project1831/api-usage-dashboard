
const LoginErrorModal = ({ handleCloseLoginErrorModal }) => {
  return (
    <>
      <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
        <div className=" h-[250px] w-[450px] bg-[#5F085A] text-white  rounded-[20px] shadow flex items-center justify-center">
          <div className="flex flex-col items-center justify-center gap-4">
            <div>Username or Password Not Recognized</div>
            <button onClick={handleCloseLoginErrorModal} className="p-4 bg-white text-[#5F085A] font-semibold text-xl rounded-2xl">Okay!</button>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginErrorModal;
