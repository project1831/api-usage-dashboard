const ProjectForm = ({ handleOpenModal }) => {
  return (
    <>
      <div className="flex flex-col justify-between gap-6">
        <div
          style={{
            fontSize: "19px",
            fontWeight: "600",
            lineHeight: "23.16px",
          }}
        >
          Create New Project
        </div>
        <div className="flex flex-row gap-4">
          <div className="w-[50%]">
            <div className="w-full ">
              <label
                for="projectname"
                class="flex-1 block text-[#666666] p-1"
                style={{
                  fontSize: "14px",
                  fontWeight: "600",
                  lineHeight: "18px",
                }}
              >
                Project Name
              </label>
              <div className="flex flex-row items-center w-full p-1 bg-white border-[1px] border-[#CCCCCC] rounded-xl justify-normal">
                <input
                  type="text"
                  id="projectname"
                  class="bg-white h-10  text-[#666666] font-bold text-sm block  p-2 w-full placeholder:text-[#00000066] placeholder:font-semibold"
                  placeholder="Enter your project name"
                  required
                />
              </div>
            </div>
          </div>
          <div className="w-[50%]">
            <div className="w-full ">
              <label
                for="adminemailid"
                class="flex-1 block text-[#666666] p-1"
                style={{
                  fontSize: "14px",
                  fontWeight: "600",
                  lineHeight: "18px",
                }}
              >
                Admin Email ID
              </label>
              <div className="flex flex-row items-center w-full p-1 bg-white border-[1px] border-[#CCCCCC] rounded-xl justify-normal">
                <input
                  type="text"
                  id="adminemailid"
                  class="bg-white h-10  text-[#666666] font-bold text-sm block  p-2 w-full placeholder:text-[#00000066] placeholder:font-semibold "
                  placeholder="Enter admin's email id"
                  required
                />
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-row gap-4">
          <div className="w-[50%]">
            <div className="w-full ">
              <label
                for="projectinitiationdate"
                class="flex-1 block text-[#666666] p-1"
                style={{
                  fontSize: "14px",
                  fontWeight: "600",
                  lineHeight: "18px",
                }}
              >
                Project Initiation Date
              </label>
              <div className="flex flex-row items-center w-full p-1 bg-white border-[1px] border-[#CCCCCC] rounded-xl justify-normal">
                <input
                  type="date"
                  id="projectinitiationdate"
                  class="bg-white h-10  text-[#666666] font-bold text-sm block  p-2 w-full placeholder:text-[#00000066] placeholder:font-medium "
                  placeholder=""
                  required
                />
              </div>
            </div>
          </div>
          <div className="w-[50%]">
            <div className="w-full ">
              <label
                for="projectterminationdate"
                class="flex-1 block text-[#666666] p-1"
                style={{
                  fontSize: "14px",
                  fontWeight: "600",
                  lineHeight: "18px",
                }}
              >
                Project Termination Date
              </label>
              <div className="flex flex-row items-center w-full p-1 bg-white border-[1px] border-[#CCCCCC] rounded-xl justify-normal">
                <input
                  type="date"
                  id="projectterminationdate"
                  class="bg-white h-10  text-[#666666] font-bold text-sm block  p-2 w-full placeholder:text-[#00000066] placeholder:font-medium "
                  placeholder=""
                  required
                />
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-row gap-4">
          <div className="w-[50%]">
            <div className="w-full ">
              <label
                for="budget"
                class="flex-1 block text-[#666666] p-1"
                style={{
                  fontSize: "14px",
                  fontWeight: "600",
                  lineHeight: "18px",
                }}
              >
                Budget
              </label>
              <div className="flex flex-row items-center w-full p-1 bg-white border-[1px] border-[#CCCCCC] rounded-xl justify-normal">
                <input
                  type="text"
                  id="budget"
                  class="bg-white h-10  text-[#666666] font-bold text-sm block  p-2 w-full placeholder:text-[#00000066] placeholder:font-semibold "
                  placeholder="Enter Budget"
                  required
                />
              </div>
            </div>
          </div>
          <div className="w-[50%]">
            <div className="w-full ">
              <label
                for="budgetthreshold"
                class="flex-1 block text-[#666666] p-1"
                style={{
                  fontSize: "14px",
                  fontWeight: "600",
                  lineHeight: "18px",
                }}
              >
                Budget Threshold
              </label>
              <div className="flex flex-row items-center w-full p-1 bg-white border-[1px] border-[#CCCCCC] rounded-xl justify-normal">
                <input
                  type="text"
                  id="budgetthreshold"
                  class="bg-white h-10  text-[#666666] font-bold text-sm block  p-2 w-full placeholder:text-[#00000066] placeholder:font-semibold "
                  placeholder="Enter Budget Threshold"
                  required
                />
              </div>
            </div>
          </div>
        </div>
        <div className="flex justify-end">
          <button
            onClick={handleOpenModal}
            className="w-[30%] p-2 bg-[#5F085A] text-white rounded-3xl"
          >
            Create
          </button>
        </div>
      </div>
    </>
  );
};

export default ProjectForm;
