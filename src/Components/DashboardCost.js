import MixBarchart from "./MixBarChart";
import PieSlider from "./PieSlider";
// import { useState, useRef, useEffect } from "react";
const DashboardCost = ({ data, sliderData }) => {
  console.log(data, "costcheck");
  
  function convertDateFormat(dateString) {
    var dateParts = dateString.split("-");
    var year = dateParts[0];
    var month = dateParts[1];
    var day = dateParts[2];
    
    var monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];

    var monthName = monthNames[parseInt(month) - 1];
    // console.log(day + monthName + "," + year)
    return day + monthName + "," + year;
  }
  

  return (
    <>
      <div className="flex flex-row justify-around w-full h-full gap-4 ">
        <div className="grid grid-rows-3 gap-2 w-fit ">
          



    {
      Object.entries(data).map(([key, arrayData], index) => (
        <div
              className="w-fit  bg-white rounded-[20px] p-6 flex flex-col"
              key={index}
            >
              <span
              style={{
                fontSize: "20px",
                fontWeight: "600",
                lineHeight: "32px",
              }}
              className="text-[#05004E]"
            >
              {key.split(":")[0].toUpperCase()}
            </span>
          <div key={index} className="h-fit w-fit">
                    <MixBarchart
                      category={[
                        {
                          category: arrayData.map((item) => ({
                            label: convertDateFormat(item.date),
                          })),
                        },
                      ]}
                      data={[
                        { data: arrayData.map((item) => ({ value: item.cost })) },
                      ]}
                      wid={true}
                    ></MixBarchart>
                  </div>

        </div>
      ))
    }
    {/* <div className="w-fit  bg-white rounded-[20px] p-6 flex flex-col"> */}
  </div>

        
        <div className=" h-fit p-6 w-fit bg-white rounded-[20px]">
          <div className="flex flex-col items-center justify-center gap-2 ">
            <div className="h-[60%] w-fit ">
              <PieSlider sliderDataValue={sliderData.budgetThreshold} />
            </div>
            <div
              className="text-[#1E1B39]"
              style={{
                fontSize: "16px",
                fontWeight: "600",
                lineHeight: "24px",
              }}
            >
              $ {sliderData.spentAmount.toFixed(3)} / ${" "}
              {sliderData.projectBudget}
            </div>
            <div>
              <button className="bg-[#380335] text-white p-3 rounded-[25px]">
                Increase Limit
              </button>
            </div>
          </div>
          <hr className="my-6 border-2"></hr>
          <div className="flex flex-col gap-6 p-2">
            <div className="flex flex-col items-center justify-center gap-2 ">
              <span
                className="text-[#615E83]"
                style={{
                  fontSize: "16px",
                  fontWeight: "400",
                  lineHeight: "18px",
                }}
              >
                Credit Grants (USD)
              </span>
              <span
                className="text-[#380335]"
                style={{
                  fontSize: "18px",
                  fontWeight: "700",
                  lineHeight: "24px",
                }}
              >
                <span className="text-[#AA0DA1]">$4.00</span> / $5.00
              </span>
            </div>
            <div className="flex items-center justify-center ">
              <progress value={70} max={100} className="w-full " />
            </div>
            <div className="flex flex-col gap-4 justify-evenly">
              <div className="flex flex-row">
                <div className="w-[50%] flex flex-row gap-2 justify-start items-center">
                  <div className="text-[#380335]">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                      className="w-6 h-6"
                    >
                      <path d="M12.75 12.75a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM7.5 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM8.25 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM9.75 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM10.5 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM12 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM12.75 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM14.25 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM15 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM16.5 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM15 12.75a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM16.5 13.5a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5Z" />
                      <path
                        fillRule="evenodd"
                        d="M6.75 2.25A.75.75 0 0 1 7.5 3v1.5h9V3A.75.75 0 0 1 18 3v1.5h.75a3 3 0 0 1 3 3v11.25a3 3 0 0 1-3 3H5.25a3 3 0 0 1-3-3V7.5a3 3 0 0 1 3-3H6V3a.75.75 0 0 1 .75-.75Zm13.5 9a1.5 1.5 0 0 0-1.5-1.5H5.25a1.5 1.5 0 0 0-1.5 1.5v7.5a1.5 1.5 0 0 0 1.5 1.5h13.5a1.5 1.5 0 0 0 1.5-1.5v-7.5Z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </div>
                  <span
                    style={{
                      fontSize: "14px",
                      fontWeight: "500",
                      lineHeight: "17.07px",
                    }}
                    className="text-[#464E5F]"
                  >
                    Recieved:
                  </span>
                </div>
                <div className="w-[50%]">
                  <span
                    style={{
                      fontSize: "14px",
                      fontWeight: "600",
                      lineHeight: "17.07px",
                    }}
                    className="text-[#AA0DA1]"
                  >
                    Feb 23,2024
                  </span>
                </div>
              </div>
              <div className="flex flex-row ">
                <div className="w-[50%] flex flex-row gap-2 justify-start items-center">
                  <div className="text-[#0FA630]">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M9 12.75 11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 0 1-1.043 3.296 3.745 3.745 0 0 1-3.296 1.043A3.745 3.745 0 0 1 12 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 0 1-3.296-1.043 3.745 3.745 0 0 1-1.043-3.296A3.745 3.745 0 0 1 3 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 0 1 1.043-3.296 3.746 3.746 0 0 1 3.296-1.043A3.746 3.746 0 0 1 12 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 0 1 3.296 1.043 3.746 3.746 0 0 1 1.043 3.296A3.745 3.745 0 0 1 21 12Z"
                      />
                    </svg>
                  </div>
                  <span
                    style={{
                      fontSize: "14px",
                      fontWeight: "500",
                      lineHeight: "17.07px",
                    }}
                    className="text-[#464E5F]"
                  >
                    Status:
                  </span>
                </div>
                <div className="w-[50%]">
                  <span
                    style={{
                      fontSize: "14px",
                      fontWeight: "600",
                      lineHeight: "17.07px",
                    }}
                    className="text-[#0FA630]"
                  >
                    Available
                  </span>
                </div>
              </div>
              <div className="flex flex-row ">
                <div className="w-[50%] flex flex-row gap-2 justify-start items-center">
                  <div className="text-[#380335]">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                      className="w-6 h-6"
                    >
                      <path d="M12 7.5a2.25 2.25 0 1 0 0 4.5 2.25 2.25 0 0 0 0-4.5Z" />
                      <path
                        fillRule="evenodd"
                        d="M1.5 4.875C1.5 3.839 2.34 3 3.375 3h17.25c1.035 0 1.875.84 1.875 1.875v9.75c0 1.036-.84 1.875-1.875 1.875H3.375A1.875 1.875 0 0 1 1.5 14.625v-9.75ZM8.25 9.75a3.75 3.75 0 1 1 7.5 0 3.75 3.75 0 0 1-7.5 0ZM18.75 9a.75.75 0 0 0-.75.75v.008c0 .414.336.75.75.75h.008a.75.75 0 0 0 .75-.75V9.75a.75.75 0 0 0-.75-.75h-.008ZM4.5 9.75A.75.75 0 0 1 5.25 9h.008a.75.75 0 0 1 .75.75v.008a.75.75 0 0 1-.75.75H5.25a.75.75 0 0 1-.75-.75V9.75Z"
                        clipRule="evenodd"
                      />
                      <path d="M2.25 18a.75.75 0 0 0 0 1.5c5.4 0 10.63.722 15.6 2.075 1.19.324 2.4-.558 2.4-1.82V18.75a.75.75 0 0 0-.75-.75H2.25Z" />
                    </svg>
                  </div>
                  <span
                    style={{
                      fontSize: "14px",
                      fontWeight: "500",
                      lineHeight: "17.07px",
                    }}
                    className="text-[#464E5F]"
                  >
                    Balance:
                  </span>
                </div>
                <div className="w-[50%]">
                  <span
                    style={{
                      fontSize: "14px",
                      fontWeight: "600",
                      lineHeight: "17.07px",
                    }}
                    className="text-[#AA0DA1]"
                  >
                    $4.00 / $5.00
                  </span>
                </div>
              </div>
              <div className="flex flex-row ">
                <div className="w-[50%] flex flex-row gap-2 justify-start items-center">
                  <div className="text-[#380335]">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                      className="w-6 h-6"
                    >
                      <path d="M12.75 12.75a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM7.5 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM8.25 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM9.75 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM10.5 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM12 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM12.75 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM14.25 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM15 17.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM16.5 15.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5ZM15 12.75a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM16.5 13.5a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5Z" />
                      <path
                        fillRule="evenodd"
                        d="M6.75 2.25A.75.75 0 0 1 7.5 3v1.5h9V3A.75.75 0 0 1 18 3v1.5h.75a3 3 0 0 1 3 3v11.25a3 3 0 0 1-3 3H5.25a3 3 0 0 1-3-3V7.5a3 3 0 0 1 3-3H6V3a.75.75 0 0 1 .75-.75Zm13.5 9a1.5 1.5 0 0 0-1.5-1.5H5.25a1.5 1.5 0 0 0-1.5 1.5v7.5a1.5 1.5 0 0 0 1.5 1.5h13.5a1.5 1.5 0 0 0 1.5-1.5v-7.5Z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </div>
                  <span
                    style={{
                      fontSize: "14px",
                      fontWeight: "500",
                      lineHeight: "17.07px",
                    }}
                    className="text-[#464E5F]"
                  >
                    Expires:
                  </span>
                </div>
                <div className="w-[50%]">
                  <span
                    style={{
                      fontSize: "14px",
                      fontWeight: "600",
                      lineHeight: "17.07px",
                    }}
                    className="text-[#AA0DA1]"
                  >
                    May 23,2024
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DashboardCost;
