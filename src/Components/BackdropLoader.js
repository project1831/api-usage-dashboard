// BackdropLoader.js
import React from 'react';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';

const BackdropLoader = () => {
  return (
    <Backdrop
      sx={{ color: '#5F085A',backgroundColor: 'rgba(194, 198, 204, 0.7)', zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={true} // Always open when rendered
      onClick={() => {}} // Empty onClick function, so clicking backdrop does nothing
    >
      <CircularProgress color="inherit" />
    </Backdrop>
  );
};

export default BackdropLoader;
