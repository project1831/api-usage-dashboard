import React from "react";
import FusionCharts from "fusioncharts";
import ReactFusioncharts from "react-fusioncharts";
import charts from "fusioncharts/fusioncharts.charts";
import ExcelExport from "fusioncharts/fusioncharts.excelexport";
//import './Barchart.css'

// Resolves charts dependancy
charts(FusionCharts);
FusionCharts.addDep(ExcelExport);

const MixBarchart = ({ category, data, wid }) => {
  const config = {
    showpercentvalues: "1",
    aligncaptionwithcanvas: "0",
    captionpadding: "0",

    showPercentValues: "0",
    bgColor: "#FFFFFF",
    exportEnabled: "1",

    // border
    showBorder: "0",
    showCanvasBorder: "0",
    usePlotGradientColor: "0",
    showPlotBorder: "0",

    pieRadius: "100",
    doughnutRadius: "70",
    use3DLighting: "0",

    paletteColors: "#5F085A",

    // Legend
    showLegend: "1",
    legendBgColor: "#ffffff",
    legendBorder: "0",
    legendBorderThickness: "0",
    legendShadow: "0",
    legendIconScale: "1",
    // legendIconBorderThickness: "0",
    legendItemFontBold: "0",
    legendItemFontSize: "12",

    // data label
    labelFontSize: "12px",
    showValues: "0",
    // valueBgColor: "#666666",

    // x-axis
    // xaxisname: "Country",
    xAxisNameFontSize: "11",
    xAxisNameFontColor: "#D54F19",
    xAxisNameAlpha: "80",
    // xAxisNameFontBold: "1",
    // xAxisNameFontItalic: "1",

    // y-axis
    // yaxisname: "Total Effective Storage (mm)",
    yAxisNameFontSize: "11",
    syAxisNameFontSize: "11",
    // yAxisNameFontColor: "#1BABC6",
    yAxisNameAlpha: "80",
    // yAxisNameFontBold: "1",
    // yAxisNameFontItalic: "1",

    // parent S axis
    // sYAxisName: "Total Water Inflow ",
    // yAxisNameFontColor: "#1BABC6",

    // Toolbar
    exportenabled: "0",
    toolbarButtonScale: "1",
    dataLoadStartMessage: "Please wait, chart is loading the data....",
  };

  // const categories = [
  //     {
  //         "category": [
  //             {
  //                 "label": "March"
  //             },
  //             {
  //                 "label": "April"
  //             },
  //             {
  //                 "label": "May"
  //             },
  //             {
  //                 "label": "June"
  //             },
  //             {
  //                 "label": "July"
  //             },

  //         ]
  //     }
  // ]

  const categories = category;

  const dataset = data;

  // const dataset = [
  //     {

  //         "data": [
  //             {
  //                 "value": "14"
  //             },
  //             {
  //                 "value": "17"
  //             },
  //             {
  //                 "value": "6"
  //             },
  //             {
  //                 "value": "16"
  //             },
  //             {
  //                 "value": "12"
  //             },

  //         ]
  //     },
  // ]

  const dataSource = {
    chart: config,
    categories: categories,
    dataset: dataset,
  };

  return (
    <>
      <div className="w-fit chartpage h-fit">
        {wid ? (
          <>
            <ReactFusioncharts
              type="mscombi2d"
              dataFormat="JSON"
              dataSource={dataSource}
              width="60%"
              //className="charts"
            />
          </>
        ) : (
          <>
            <ReactFusioncharts
              type="mscombi2d"
              dataFormat="JSON"
              dataSource={dataSource}
              //className="charts"
            />
          </>
        )}
      </div>
    </>
  );
};

export default MixBarchart;
