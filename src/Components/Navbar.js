import profile from "../Assets/profile.svg";
const Navbar = ({
  dashboard,
  handleDashboard,
  project,
  handleProject,
  settings,
  handleSettings,
}) => {
  return (
    <>
      <div className="w-full text-white bg-white ">
        <div
          className={`py-10 ${
            dashboard ? "rounded-br-[36px]" : ""
          }  bg-[#5F085A] h-full rounded-t-[32px]  flex flex-col justify-evenl px-[10%] py-[7%]`}
        >
          <div className="flex flex-row justify-between w-full ">
            <div className="">
              <span class="status text-[#00FF38]">●</span>
              <span
                style={{
                  fontSize: "10px",
                  fontWeight: "500",
                  lineHeight: "15px",
                }}
              >
                Active
              </span>
            </div>
            <div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M6.75 12a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM12.75 12a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM18.75 12a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0Z"
                />
              </svg>
            </div>
          </div>
          <div className="flex flex-col items-center justify-center w-full h-full gap-1">
            <div className="">
              <div>
                <img className="" src={profile} alt="profile"></img>
              </div>
            </div>
            <div
              style={{
                fontSize: "12px",
                fontWeight: "700",
                lineHeight: "18px",
              }}
              className="pt-6 font-sans"
            >
              USER
            </div>
            <div
              className="font-sans"
              style={{
                fontSize: "14px",
                fontWeight: "400",
                lineHeight: "19.07px",
              }}
            >
              admin@pwc.com
            </div>
          </div>
        </div>
      </div>
      <div className="flex flex-col w-full ">
        <div
          onClick={handleDashboard}
          className={` ${dashboard ? "bg-[#5F085A]" : "bg-white"} ml-4`}
        >
          <div
            style={{
              fontSize: "15px",
              fontWeight: "500",
              lineHeight: "18.29px",
            }}
            className={`p-4 px-6 flex flex-row justify-start items-center gap-2 ${
              dashboard
                ? "bg-white text-[#5F085A]  rounded-l-[36px] border-2 border-white"
                : " text-white bg-[#5F085A]"
            }  ${project ? "rounded-br-[36px]" : ""} `}
          >
            <div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
                className="w-6 h-6"
              >
                <path
                  fillRule="evenodd"
                  d="M3 6a3 3 0 0 1 3-3h2.25a3 3 0 0 1 3 3v2.25a3 3 0 0 1-3 3H6a3 3 0 0 1-3-3V6Zm9.75 0a3 3 0 0 1 3-3H18a3 3 0 0 1 3 3v2.25a3 3 0 0 1-3 3h-2.25a3 3 0 0 1-3-3V6ZM3 15.75a3 3 0 0 1 3-3h2.25a3 3 0 0 1 3 3V18a3 3 0 0 1-3 3H6a3 3 0 0 1-3-3v-2.25Zm9.75 0a3 3 0 0 1 3-3H18a3 3 0 0 1 3 3V18a3 3 0 0 1-3 3h-2.25a3 3 0 0 1-3-3v-2.25Z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <span>Dashboard</span>
          </div>
        </div>
        <div
          onClick={handleProject}
          className={`${project ? "bg-[#5F085A]" : "bg-white"} ml-4`}
        >
          <div
            style={{
              fontSize: "15px",
              fontWeight: "500",
              lineHeight: "18.29px",
            }}
            className={`p-4 px-6 gap-2 flex flex-row justify-start items-center ${
              project
                ? "bg-white text-[#5F085A]  rounded-l-[36px] border-2 border-white"
                : " text-white bg-[#5F085A]"
            } ${dashboard ? "rounded-tr-[36px] " : ""} ${
              settings ? "rounded-br-[36px]" : ""
            }`}
          >
            <div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
                className="w-6 h-6"
              >
                <path
                  fillRule="evenodd"
                  d="M2.25 5.25a3 3 0 0 1 3-3h13.5a3 3 0 0 1 3 3V15a3 3 0 0 1-3 3h-3v.257c0 .597.237 1.17.659 1.591l.621.622a.75.75 0 0 1-.53 1.28h-9a.75.75 0 0 1-.53-1.28l.621-.622a2.25 2.25 0 0 0 .659-1.59V18h-3a3 3 0 0 1-3-3V5.25Zm1.5 0v7.5a1.5 1.5 0 0 0 1.5 1.5h13.5a1.5 1.5 0 0 0 1.5-1.5v-7.5a1.5 1.5 0 0 0-1.5-1.5H5.25a1.5 1.5 0 0 0-1.5 1.5Z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <span>Project Creation</span>
          </div>
        </div>
        <div
          onClick={handleSettings}
          className={`${settings ? "bg-[#5F085A]" : "bg-white"} ml-4 `}
        >
          <div
            style={{
              fontSize: "15px",
              fontWeight: "500",
              lineHeight: "18.29px",
            }}
            className={` p-4 px-6 flex flex-row justify-start items-center gap-2 ${
              settings
                ? "bg-white text-[#5F085A]  rounded-l-[36px] border-2 border-white "
                : " text-white bg-[#5F085A]"
            } ${project ? "rounded-tr-[36px] " : ""} `}
          >
            <div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
                className="w-6 h-6"
              >
                <path
                  fillRule="evenodd"
                  d="M11.078 2.25c-.917 0-1.699.663-1.85 1.567L9.05 4.889c-.02.12-.115.26-.297.348a7.493 7.493 0 0 0-.986.57c-.166.115-.334.126-.45.083L6.3 5.508a1.875 1.875 0 0 0-2.282.819l-.922 1.597a1.875 1.875 0 0 0 .432 2.385l.84.692c.095.078.17.229.154.43a7.598 7.598 0 0 0 0 1.139c.015.2-.059.352-.153.43l-.841.692a1.875 1.875 0 0 0-.432 2.385l.922 1.597a1.875 1.875 0 0 0 2.282.818l1.019-.382c.115-.043.283-.031.45.082.312.214.641.405.985.57.182.088.277.228.297.35l.178 1.071c.151.904.933 1.567 1.85 1.567h1.844c.916 0 1.699-.663 1.85-1.567l.178-1.072c.02-.12.114-.26.297-.349.344-.165.673-.356.985-.57.167-.114.335-.125.45-.082l1.02.382a1.875 1.875 0 0 0 2.28-.819l.923-1.597a1.875 1.875 0 0 0-.432-2.385l-.84-.692c-.095-.078-.17-.229-.154-.43a7.614 7.614 0 0 0 0-1.139c-.016-.2.059-.352.153-.43l.84-.692c.708-.582.891-1.59.433-2.385l-.922-1.597a1.875 1.875 0 0 0-2.282-.818l-1.02.382c-.114.043-.282.031-.449-.083a7.49 7.49 0 0 0-.985-.57c-.183-.087-.277-.227-.297-.348l-.179-1.072a1.875 1.875 0 0 0-1.85-1.567h-1.843ZM12 15.75a3.75 3.75 0 1 0 0-7.5 3.75 3.75 0 0 0 0 7.5Z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <span>Settings</span>
          </div>
        </div>
      </div>
      <div className={`${settings ? "bg-white" : ""} `}>
        <div
          class={`   pb-8 ${
            settings ? " bg-[#5F085A] rounded-tr-[36px] border-[0px] " : ""
          }`}
        ></div>
      </div>
    </>
  );
};
export default Navbar;
