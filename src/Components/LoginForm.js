import logo from "../Assets/apilogo.svg";
import phone from "../Assets/phone.svg";
import lock from "../Assets/lock.svg";
import closeeye from "../Assets/invisibleeye.svg";
import google from "../Assets/google.svg";
import apple from "../Assets/apple.svg";
import facebook from "../Assets/facebook.svg";
import {useState,} from 'react';

const LoginForm = ({ goToSignUpPage, goToDashboard ,loginStatus}) => {
  const [username,setUsername] = useState("");
  const [password,setPassword] = useState("");

  const loginData = {
    username: username,
    password: password
  }
  const handleLogin = () => {
    loginStatus(loginData)
  }
  return (
    <>
      <div className="flex flex-row h-[9.81%] gap-2 justify-start items-start">
        <div
          className="p-2 "
          style={{
            boxShadow: "0px 3px 3px 0px #F2F2F2",
            border: "2px ",
            borderRadius: "50px",
          }}
        >
          <img src={logo} alt="logo"></img>
        </div>
        <div
          className="flex flex-col justify-center text-[#0B0C0C] "
          style={{
            fontSize: "26px",
            fontWeight: "600",
            lineHeight: "28px",
            textAlign: "left",
          }}
        >
          <span>API-Usage</span>
          <span>Dashboard</span>
        </div>
      </div>
      <div className="flex flex-col h-[83.26%] pt-[6.92%]">
        <div
          className="text-black"
          style={{
            fontSize: "30px",
            fontWeight: "500",
            lineHeight: "36.7px",
          }}
        >
          Welcome User
        </div>
        <div
          className="flex flex-col pt-10 w-[71.13%]"
          style={{
            fontSize: "16px",
            fontWeight: "400",
            lineHeight: "19.5px",
          }}
        >
          <span>If you don’t have an account register</span>
          <span className="pt-2 ">
            You can{" "}
            <span
              style={{
                color: "purple",
                fontWeight: "bold",
                cursor: "pointer",
              }}
              onClick={goToSignUpPage}
            >
              Register here!{" "}
            </span>
          </span>
        </div>
        <div className="gap-5 pt-10">
          <div className="flex flex-col gap-5">
            <div>
              <label
                for="phoneNo"
                class="flex-1 block text-black"
                style={{
                  fontSize: "13px",
                  fontWeight: "500",
                  lineHeight: "15.8px",
                }}
              >
                Phone No
              </label>
              <div className="flex flex-row w-full border-b-2 border-black ">
                <img src={phone} alt="phone" className=""></img>
                <input
                  type="text"
                  id="phoneNo"
                  class="bg-white   text-black font-bold text-sm block  p-2 w-full placeholder:text-[#00000066] placeholder:font-medium "
                  placeholder=" Enter your phone no"
                  required
                  onChange={(e)=>{setUsername(e.target.value)}}
                />
              </div>
            </div>
            <div>
              <label
                for="password"
                class="flex-1 block text-black"
                style={{
                  fontSize: "13px",
                  fontWeight: "500",
                  lineHeight: "15.8px",
                }}
              >
                Password
              </label>
              <div className="flex flex-row w-full border-b-2 border-black justify-evenly">
                <img src={lock} alt="lock"></img>
                <input
                  type="password"
                  id="password"
                  class="bg-white   text-black font-bold text-sm w-full  p-2  placeholder:text-[#00000066] placeholder:font-medium "
                  placeholder=" Enter your Password"
                  required
                  onChange={(e)=>{setPassword(e.target.value)}}
                />
                <img src={closeeye} alt="closedeye" className=""></img>
              </div>
            </div>
          </div>
          <div className="pt-10">
            <button
              onClick={handleLogin}
              className="bg-[#5F085A] text-white w-full p-2 rounded-3xl"
            >
              {" "}
              Enter Now
            </button>
            <div className="flex flex-row justify-between w-full pt-2 text-xs">
              <div>
                <input type="checkbox" id="rememberme" className="m-1"></input>
                <label for="rememberme">Remember me</label>
              </div>
              <div>Forgot Password?</div>
            </div>
          </div>
        </div>
        <div className="flex flex-col items-center justify-center w-full">
          <div
            className="text-[#B5B5B5] p-4"
            style={{
              fontSize: "16px",
              fontWeight: "500",
              lineHeight: "19.5px",
            }}
          >
            or continue with
          </div>
          <div className="flex flex-row gap-4">
            <img src={google} alt="Gooogle"></img>
            <img src={facebook} alt="Facebook"></img>
            <img src={apple} alt="Apple"></img>
          </div>
        </div>
      </div>
    </>
  );
};
export default LoginForm;
