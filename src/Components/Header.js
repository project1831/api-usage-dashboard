import logo from "../Assets/apilogo.svg";
import avatar from "../Assets/avatar.svg";
import logout from "../Assets/logout.svg";

const Header = ({ goToLoginPage }) => {
  return (
    <>
      <div className="flex flex-row ">
        <div
          className="w-16 h-16 p-3 mr-4 "
          style={{
            boxShadow: "0px 3px 3px 0px #F2F2F2",
            border: "1px ",
            borderRadius: "50px",
          }}
        >
          <img src={logo} alt="logo"></img>
        </div>
        <div
          className="flex flex-col justify-center text-[#0B0C0C] "
          style={{
            fontSize: "26px",
            fontWeight: "600",
            lineHeight: "28px",
            textAlign: "left",
          }}
        >
          <span>API-Usage</span>
          <span>Dashboard</span>
        </div>
      </div>
      <div className="flex flex-row items-center gap-2 ">
        <div className="flex flex-row gap-2">
          <img src={avatar} alt="avatar"></img>
          <span>Welcome User</span>
        </div>
        <div class="w-0.5 h-6 bg-[#D9D9D9] mx-2"></div>

        <div onClick={goToLoginPage} className="flex flex-row gap-2">
          <span>Logout</span>
          <img src={logout} alt="logout"></img>
        </div>
      </div>
    </>
  );
};

export default Header;
