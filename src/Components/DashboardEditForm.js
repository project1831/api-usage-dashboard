import { useState, useEffect } from "react";
import axios from "axios";

const DashboardEditForm = ({ goToDashboardSubmit, projectPayLoad }) => {
  const [name, setName] = useState(projectPayLoad.projectName);
  const [startdate, setStartdate] = useState(projectPayLoad.startDate);
  const [enddate, setEnddate] = useState(projectPayLoad.endDate);
  const [isOpen, setIsOpen] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");

  const [items, setItems] = useState([]);
  const [filteredItems, setFilteredItems] = useState([]);
  const token = localStorage.getItem("token")
  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };
  const fetchData = async () => {
    try {
      const response = await axios.post(
        "https://api-usage-dashboard-backend-python.azurewebsites.net/get-projects-names",
        { adminEmailId: "bishwayan.saha@pwc.com" },
        {
          headers: {
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`,
          },
        }
      );
      // console.log(response.data.projects);
      setItems(response.data.projects);
      setFilteredItems(response.data.projects);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  // Call fetchData on component mount
  useEffect(() => {
    fetchData();
  });

  const handleInputChange = (e) => {
    const term = e.target.value.toLowerCase();
    setSearchTerm(term);

    // Filter items based on the search term
    const filtered = term
      ? items.filter((item) => item.toLowerCase().includes(term))
      : items;

    setFilteredItems(filtered);
  };

  const nameChange = (data) => {
    setName(data);
    // console.log(name);
    setIsOpen(false);
  };

  const startChange = (e) => {
    setStartdate(e.target.value);
  };

  const endChange = (e) => {
    setEnddate(e.target.value);
  };

  const payLoad = {
    startDate: startdate,
    endDate: enddate,
    projectName: name,
  };

  // console.log(payLoad);
  const handleDashboardSubmit = (e) => {
    goToDashboardSubmit(payLoad);
  };
  return (
    <>
      <div className="flex flex-col justify-between gap-4">
        <div
          style={{
            fontSize: "19px",
            fontWeight: "600",
            lineHeight: "23.16px",
          }}
        >
          Enter Your Project Details To Continue
        </div>
        <div></div>

        <div className="flex flex-row items-end justify-between w-full gap-4">
          <div className="w-full">
            <div className="flex flex-col">
              <label
                for="projectinitiationdate"
                class="flex-1 block text-[#666666] p-1"
                style={{
                  fontSize: "14px",
                  fontWeight: "600",
                  lineHeight: "18px",
                }}
              >
                Select Project Name
              </label>
              <div className="relative w-full p-1 group items-center  bg-white border-[1px] border-[#CCCCCC] rounded-xl justify-normal">
                <button
                  id="dropdown-button"
                  onClick={toggleDropdown}
                  className="inline-flex justify-between w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-blue-500"
                >
                  <span
                    className="mr-2"
                    style={{
                      fontSize: "16px",
                      fontWeight: "600",
                      lineHeight: "23.16px",
                    }}
                  >
                    {name ? <>{name}</> : <>Enter Project Name</>}
                  </span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M3.75 3v11.25A2.25 2.25 0 0 0 6 16.5h2.25M3.75 3h-1.5m1.5 0h16.5m0 0h1.5m-1.5 0v11.25A2.25 2.25 0 0 1 18 16.5h-2.25m-7.5 0h7.5m-7.5 0-1 3m8.5-3 1 3m0 0 .5 1.5m-.5-1.5h-9.5m0 0-.5 1.5M9 11.25v1.5M12 9v3.75m3-6v6"
                    />
                  </svg>
                </button>
                <div
                  id="dropdown-menu"
                  className={`${
                    isOpen ? "" : "hidden"
                  } w-full absolute right-0 mt-2 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 p-1 space-y-1`}
                >
                  {/* Search input */}
                  <div className="flex flex-row px-4 py-2 text-gray-800 border border-gray-300 rounded-md focus:outline-none">
                    <input
                      id="search-input"
                      className="w-full "
                      type="text"
                      placeholder="Search items"
                      autoComplete="off"
                      value={searchTerm}
                      onChange={handleInputChange}
                    />
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-8 h-8"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="m21 21-5.197-5.197m0 0A7.5 7.5 0 1 0 5.196 5.196a7.5 7.5 0 0 0 10.607 10.607Z"
                      />
                    </svg>
                  </div>
                  {/* Display filtered items */}
                  {filteredItems.map((item, index) => (
                    <div
                      key={index}
                      onClick={() => nameChange(item)}
                      className="block px-4 py-2 text-gray-700 rounded-md cursor-pointer hover:bg-gray-100 active:bg-blue-100"
                    >
                      {item}
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>

          <div className="w-full">
            <div className="w-full ">
              <label
                for="projectinitiationdate"
                class="flex-1 block text-[#666666] p-1"
                style={{
                  fontSize: "14px",
                  fontWeight: "600",
                  lineHeight: "18px",
                }}
              >
                Project Initiation Date
              </label>
              <div className="flex flex-row items-center w-full p-1 bg-white border-[1px] border-[#CCCCCC] rounded-xl justify-normal">
                <input
                  type="date"
                  id="projectinitiationdate"
                  class="bg-white h-10  text-[#666666] font-bold text-sm block  p-2 w-full placeholder:text-[#00000066] placeholder:font-medium "
                  placeholder=""
                  required
                  value={startdate}
                  onChange={startChange}
                />
              </div>
            </div>
          </div>
          <div className="w-full">
            <div className="w-full ">
              <label
                for="projectterminationdate"
                class="flex-1 block text-[#666666] p-1"
                style={{
                  fontSize: "14px",
                  fontWeight: "600",
                  lineHeight: "18px",
                }}
              >
                Project Termination Date
              </label>
              <div className="flex flex-row items-center w-full p-1 bg-white border-[1px] border-[#CCCCCC] rounded-xl justify-normal">
                <input
                  type="date"
                  id="projectterminationdate"
                  class="bg-white h-10  text-[#666666] font-bold text-sm block  p-2 w-full placeholder:text-[#00000066] placeholder:font-medium "
                  placeholder=""
                  value={enddate}
                  required
                  onChange={endChange}
                />
              </div>
            </div>
          </div>
          <div className="flex">
            <button
              onClick={handleDashboardSubmit}
              className=" h-12 w-16 bg-[#5F085A] text-white rounded-3xl flex p-2 justify-center "
            >
              <svg
                width="30"
                height="30"
                viewBox="0 0 25 25"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  clip-rule="evenodd"
                  d="M10.7918 4.44446C9.01998 4.44446 7.32076 5.1483 6.06791 6.40115C4.81506 7.654 4.11122 9.35322 4.11122 11.125C4.11122 12.8968 4.81506 14.596 6.06791 15.8489C7.32076 17.1017 9.01998 17.8056 10.7918 17.8056C12.5636 17.8056 14.2628 17.1017 15.5156 15.8489C16.7685 14.596 17.4723 12.8968 17.4723 11.125C17.4723 9.35322 16.7685 7.654 15.5156 6.40115C14.2628 5.1483 12.5636 4.44446 10.7918 4.44446ZM2.05566 11.125C2.05578 9.73196 2.38904 8.35913 3.02763 7.12106C3.66622 5.88299 4.59162 4.81558 5.72663 4.0079C6.86164 3.20021 8.17335 2.67567 9.55232 2.47802C10.9313 2.28038 12.3375 2.41537 13.6537 2.87173C14.9699 3.3281 16.1579 4.0926 17.1185 5.10145C18.0791 6.11031 18.7846 7.33427 19.176 8.67121C19.5674 10.0082 19.6334 11.4193 19.3685 12.787C19.1036 14.1546 18.5155 15.4391 17.6532 16.5332L21.4067 20.2866C21.5939 20.4805 21.6975 20.7401 21.6951 21.0096C21.6928 21.279 21.5847 21.5368 21.3941 21.7274C21.2036 21.9179 20.9458 22.026 20.6763 22.0284C20.4068 22.0307 20.1472 21.9271 19.9534 21.7399L16.1999 17.9865C14.9117 19.0019 13.3637 19.6342 11.7329 19.8109C10.1022 19.9876 8.45463 19.7016 6.97882 18.9857C5.503 18.2698 4.25856 17.1528 3.3879 15.7627C2.51725 14.3725 2.05555 12.7653 2.05566 11.125ZM9.764 7.52779C9.764 7.25521 9.87228 6.99379 10.065 6.80105C10.2578 6.6083 10.5192 6.50002 10.7918 6.50002C12.0184 6.50002 13.1948 6.98729 14.0621 7.85465C14.9295 8.722 15.4168 9.89839 15.4168 11.125C15.4168 11.3976 15.3085 11.659 15.1157 11.8518C14.923 12.0445 14.6616 12.1528 14.389 12.1528C14.1164 12.1528 13.855 12.0445 13.6622 11.8518C13.4695 11.659 13.3612 11.3976 13.3612 11.125C13.3612 10.4436 13.0905 9.79001 12.6086 9.30814C12.1268 8.82628 11.4732 8.55557 10.7918 8.55557C10.5192 8.55557 10.2578 8.44729 10.065 8.25454C9.87228 8.0618 9.764 7.80038 9.764 7.52779Z"
                  fill="white"
                />
              </svg>
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default DashboardEditForm;
