import { useState } from "react";
import { RoundSlider } from "mz-react-round-slider";

const PieSlider = (sliderDataValue) => {
  
  const [pointers] = useState([
    {
      value:  sliderDataValue.sliderDataValue,
      radius: 0,
    },
  ]);

  return (
    <RoundSlider
      min={0}
      max={100}
      step={0.001}
      arrowStep={1}
      round={2}
      pathStartAngle={130}
      pathEndAngle={410}
      pathRadius={"100"}
      pathThickness={15}
      pathBgColor={"#F0E5FC"}
      pathInnerBgColor={"white"}
      pathInnerBgFull={true}
      connectionBgColor={"#5F085A"}
      textFontSize={14}
      textSuffix={"%"}
      textPrefix={"Threshold Limit : "}
      textBetween=""
      pointers={pointers}
      keyboardDisabled={true}
      data={60}
      disabled={true}
      connectionBgColorDisabled="#5F085A"
    />
  );
};

export default PieSlider;
