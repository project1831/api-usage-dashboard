import success from "../Assets/success.svg";
const ProjectModal = ({ handleCloseModal }) => {
  return (
    <>
      <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
        <div className=" h-[345px] w-[645px] bg-[#5F085A] text-white  rounded-[20px] shadow flex items-center justify-center">
          <div className="flex flex-col items-center justify-center gap-4">
            <div className="">
              <img
                src={success}
                alt="success"
                className="bg-[#FAFAFA] h-24 w-24 flex justify-center items-center rounded-[50px] p-2"
              ></img>
            </div>
            <div className="flex flex-col items-center justify-center">
              <div
                style={{
                  fontSize: "30px",
                  fontWeight: "600",
                  lineHeight: "30px",
                }}
              >
                Project Name
              </div>
              <div
                style={{
                  fontSize: "14px",
                  fontWeight: "400",
                  lineHeight: "20px",
                }}
              >
                Created Successfully
              </div>
            </div>
            <div>
              <button
                onClick={handleCloseModal}
                className="bg-white text-[#5F085A] px-4 py-2 rounded-2xl"
                style={{
                  fontSize: "14px",
                  fontWeight: "600",
                  lineHeight: "20px",
                }}
              >
                View Projects
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProjectModal;
