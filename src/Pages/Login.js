// Login.js
import api from "../Assets/apiimg.svg";
import { useNavigate } from "react-router-dom";
import LoginForm from "../Components/LoginForm";
import { useEffect, useState } from "react";
import BackdropLoader from "../Components/BackdropLoader";
import axios from "axios";
import LoginErrorModal from "../Components/LoginErrorModal";

function Login() {
  const navigate = useNavigate();
  const [loader, setLoader] = useState(false);

  const goToSignUpPage = () => {
    navigate("/signup"); // Path to your signup page
  };

  const goToDashboard = () => {
    navigate("/dashboard");
    setTimeout(() => {
      
      setLoader(false);
    }, 2000);
  };
  const token = localStorage.getItem("token")
  
  useEffect(() => {
    if(token != null){
      navigate("/dashboard");
    }
  }, [navigate, token]);

  const loginStatus = async (data) => {
  
    if (data.username != null && data.password!=null) {
      setLoader(true);
    }
    try {
      const response = await axios.post(
        "https://feature-email.azurewebsites.net/api/v1/auth/login",
        {
          username: data.username,
          password: data.password,
          appName: "Api-Usage-Deashboard",
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if((await response).status === 200){
        localStorage.setItem("token", response.data.token);
        localStorage.setItem("refreshToken", response.data.refreshToken);
        setLoader(false);
        goToDashboard();
      }
      
      
    } catch (error) {
      console.error("Error fetching data:", error);
      setLoader(false);
        handleOpenLoginErrorModal();
        
    }
  };

  const [showLoginErrorModal, setShowLoginErrorModal] = useState(false);
  const handleOpenLoginErrorModal = () => {
    setShowLoginErrorModal(true);
  };

  const handleCloseLoginErrorModal = () => {
    setShowLoginErrorModal(false);
  };

  return (
    <>
      <div className="flex flex-row h-screen justify-evenly">
        <div className="w-[49.3%] h-screen">
          <div className=" pt-[3.62%] pr-[15.49%] pl-[23.52%] h-[100%]   pb-[12.68%] flex flex-col justify-evenly">
            <LoginForm
              goToSignUpPage={goToSignUpPage}
              goToDashboard={goToDashboard}
              loginStatus={loginStatus}
            />
          </div>
        </div>
        <div className="w-[50.69%] h-full">
          <div className="pr-[3.43%] pt-[2.17%] pb-[12.68%] pl-[0%]">
            <img src={api} alt="API_Image" />
          </div>
        </div>
      </div>
      {loader && <BackdropLoader />}
      {showLoginErrorModal && <LoginErrorModal handleCloseLoginErrorModal={handleCloseLoginErrorModal}/>}
    </>
  );
}
export default Login;
