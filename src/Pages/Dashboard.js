import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import Header from "../Components/Header";
import Navbar from "../Components/Navbar";
import Dashboardform from "../Components/Dashboardform";
import DashboardView from "../Components/DashboardView";
import ProjectForm from "../Components/ProjectForm";
import ProjectModal from "../Components/ProjectModal";
import DashboardEditForm from "../Components/DashboardEditForm";
import BackdropLoader from "../Components/BackdropLoader"

function Dashboard() {
  const [dashboard, setDashboard] = useState(true);
  const [project, setProject] = useState(false);
  const [responseData, setResponseData] = useState(null);
  const [settings, setSettings] = useState(false);
  const [dashboardSubmit, setDashboardSubmit] = useState(false);
  const [cost, setCost] = useState(false);
  const [activity, setActivity] = useState(false);
  const [projectPayLoad, setProjectPayLoad] = useState("");
  const [dashboardEdit, setDashboardEdit] = useState(false);
  const [searchButton, setSearchButton] = useState(false);
  const [loader,setLoader] = useState(false);

  const navigate = useNavigate();

  const goToLoginPage = () => {
    localStorage.removeItem("token")
    localStorage.removeItem("refreshToken")
    navigate("/"); // Path to your signup page
  };

  const [sliderData, setSliderData] = useState(null);
  const [slider,setSlider] = useState(false);
  const token = localStorage.getItem("token")
 

  const SliderData = async (projectname) => {
    try {
      const response = await axios.post(
        "https://api-usage-dashboard-backend-python.azurewebsites.net/get-project-details",
        {projectName: projectname, },
        {
          headers: {
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`,
          },
        }
      );
        setSliderData(response.data);
        setSlider(true);
    } catch (error) {
      console.error("Error fetching data:", error);
      setSlider(false);
      alert("Input values do not match. No records found for this input.");
    }
  }

  const goToDashboardSubmit = async (payLoad) => {
    // const payload = {
    //   startDate: "2024-03-24",
    //   endDate: "2024-03-28",
    //   projectName: "Test 12345",
    // };
    setProjectPayLoad(payLoad);
    setLoader(true);
    SliderData(payLoad.projectName);
    try {
      const response = await axios.post(
        "https://api-usage-dashboard-backend-python.azurewebsites.net/price",
        payLoad,
        {
          headers: {
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`,
          },
        }
      );

      setResponseData(response.data);
      setDashboardSubmit(true);
      setCost(true);
      setActivity(false);
      setSearchButton(true);
      setLoader(false);
    } catch (error) {
      console.error("Error fetching data:", error);
      setLoader(false);
      alert("Input values do not match. No records found for this input.");
    }
  };

  const [showModal, setShowModal] = useState(false);
  const handleOpenModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const goToCost = () => {
    setCost(true);
    setActivity(false);
    setSearchButton(true);
  };
  const goToActivity = () => {
    setCost(false);
    setActivity(true);
    setDashboardEdit(false);
    setSearchButton(false);
  };

  const handleDashboard = () => {
    setDashboard(true);
    setProject(false);
    setSettings(false);
    setDashboardSubmit(false);
  };
  const handleProject = () => {
    setDashboard(false);
    setProject(true);
    setSettings(false);
    setDashboardSubmit(false);
  };
  const handleSettings = () => {
    setDashboard(false);
    setProject(false);
    setSettings(true);
    setDashboardSubmit(false);
  };
  
  useEffect(() => {
    console.log("Response Data:", responseData);
  }, [responseData]);
  useEffect(() => {
    if(token == null){
      navigate("/");
    }
  }, [navigate, token]);
  

 

  
  

  return (
   <>
   {
    token &&
    <>
    
    <div className="flex flex-col w-screen h-screen">
      <div className="flex flex-row w-screen justify-between text-black bg-white py-[1.3%] px-[4%]">
        <Header goToLoginPage={goToLoginPage}></Header>
      </div>
      <div className="w-screen  px-[1.875%] bg-[#FAFAFA] py-[1.1%]">
        <div className="flex flex-row   bg-white rounded-[32px] py-[3.13%] px-[1.875%]">
          <div className="relative w-[20%]  bg-[#5F085A] text-white rounded-[32px] ">
            <Navbar
              dashboard={dashboard}
              handleDashboard={handleDashboard}
              project={project}
              handleProject={handleProject}
              settings={settings}
              handleSettings={handleSettings}
            ></Navbar>
          </div>
          <div className=" w-[80%]  ml-[2.7%] ">
            {dashboard ? (
              dashboardSubmit && slider ? (
                <>
                  <div className="relative flex flex-col w-full gap-8">
                    {searchButton ? (
                      <>
                        <button
                          className="absolute z-20 h-10 w-10 flex justify-center items-center right-0 p-2 m-2 text-white bg-[#5F085A] rounded-full -top-5"
                          onClick={() => {
                            setDashboardEdit(!dashboardEdit);
                          }}
                        >
                          {dashboardEdit ? (
                            <>
                              <svg
                                width="18"
                                height="18"
                                viewBox="0 0 17 13"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M8.50005 12.3055L16.162 0.565959H0.838135L8.50005 12.3055Z"
                                  fill="white"
                                />
                              </svg>
                            </>
                          ) : (
                            <>
                              <svg
                                width="25"
                                height="25"
                                viewBox="0 0 25 25"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  fill-rule="evenodd"
                                  clip-rule="evenodd"
                                  d="M10.7918 4.44446C9.01998 4.44446 7.32076 5.1483 6.06791 6.40115C4.81506 7.654 4.11122 9.35322 4.11122 11.125C4.11122 12.8968 4.81506 14.596 6.06791 15.8489C7.32076 17.1017 9.01998 17.8056 10.7918 17.8056C12.5636 17.8056 14.2628 17.1017 15.5156 15.8489C16.7685 14.596 17.4723 12.8968 17.4723 11.125C17.4723 9.35322 16.7685 7.654 15.5156 6.40115C14.2628 5.1483 12.5636 4.44446 10.7918 4.44446ZM2.05566 11.125C2.05578 9.73196 2.38904 8.35913 3.02763 7.12106C3.66622 5.88299 4.59162 4.81558 5.72663 4.0079C6.86164 3.20021 8.17335 2.67567 9.55232 2.47802C10.9313 2.28038 12.3375 2.41537 13.6537 2.87173C14.9699 3.3281 16.1579 4.0926 17.1185 5.10145C18.0791 6.11031 18.7846 7.33427 19.176 8.67121C19.5674 10.0082 19.6334 11.4193 19.3685 12.787C19.1036 14.1546 18.5155 15.4391 17.6532 16.5332L21.4067 20.2866C21.5939 20.4805 21.6975 20.7401 21.6951 21.0096C21.6928 21.279 21.5847 21.5368 21.3941 21.7274C21.2036 21.9179 20.9458 22.026 20.6763 22.0284C20.4068 22.0307 20.1472 21.9271 19.9534 21.7399L16.1999 17.9865C14.9117 19.0019 13.3637 19.6342 11.7329 19.8109C10.1022 19.9876 8.45463 19.7016 6.97882 18.9857C5.503 18.2698 4.25856 17.1528 3.3879 15.7627C2.51725 14.3725 2.05555 12.7653 2.05566 11.125ZM9.764 7.52779C9.764 7.25521 9.87228 6.99379 10.065 6.80105C10.2578 6.6083 10.5192 6.50002 10.7918 6.50002C12.0184 6.50002 13.1948 6.98729 14.0621 7.85465C14.9295 8.722 15.4168 9.89839 15.4168 11.125C15.4168 11.3976 15.3085 11.659 15.1157 11.8518C14.923 12.0445 14.6616 12.1528 14.389 12.1528C14.1164 12.1528 13.855 12.0445 13.6622 11.8518C13.4695 11.659 13.3612 11.3976 13.3612 11.125C13.3612 10.4436 13.0905 9.79001 12.6086 9.30814C12.1268 8.82628 11.4732 8.55557 10.7918 8.55557C10.5192 8.55557 10.2578 8.44729 10.065 8.25454C9.87228 8.0618 9.764 7.80038 9.764 7.52779Z"
                                  fill="white"
                                />
                              </svg>
                            </>
                          )}
                        </button>
                      </>
                    ) : (
                      <></>
                    )}
                    {dashboardEdit ? (
                      <>
                        {" "}
                        <div className=" z-10  bg-[#FAFAFA] rounded-[18px]  pl-[3.8%] pr-[2%] py-[3%]">
                          {" "}
                          <DashboardEditForm
                            projectPayLoad={projectPayLoad}
                            goToDashboardSubmit={goToDashboardSubmit}
                          />{" "}
                        </div>{" "}
                      </>
                    ) : (
                      <></>
                    )}
                    <div className=" bg-[#FAFAFA] rounded-[18px]  pl-[3.8%] pr-[2%] py-[3%]">
                      <DashboardView
                        goToCost={goToCost}
                        data={responseData}
                        cost={cost}
                        goToActivity={goToActivity}
                        activity={activity}
                        projectPayLoad={projectPayLoad}
                        className=""
                        sliderData={sliderData}
                      ></DashboardView>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <div className="h-screen bg-[#FAFAFA] rounded-[18px] pl-[3.8%] pr-[2%] py-[3%]">
                    {" "}
                    <Dashboardform
                      goToDashboardSubmit={goToDashboardSubmit}
                    ></Dashboardform>
                  </div>
                </>
              )
            ) : (
              <></>
            )}

            {project ? (
              <>
                <div className="h-screen bg-[#FAFAFA] rounded-[18px] pl-[3.8%] pr-[2%] py-[3%]">
                  <ProjectForm
                    handleOpenModal={handleOpenModal}
                  ></ProjectForm>
                </div>
              </>
            ) : (
              <></>
            )}
            {settings ? (
              <>
                <div className="h-screen bg-[#FAFAFA] rounded-[18px] pl-[3.8%] pr-[2%] py-[3%]">
                  Settings
                </div>
              </>
            ) : (
              <></>
            )}
          </div>
        </div>
      </div>
    </div>
    {showModal && (
      <>
        <ProjectModal handleCloseModal={handleCloseModal}></ProjectModal>
      </>
    )}
    {loader && (
      <>
      <BackdropLoader/>
      </>
    )}
  </>
   }
   </>
  );
}

export default Dashboard;
